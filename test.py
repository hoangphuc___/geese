import torch
from kaggle_environments.envs.hungry_geese.hungry_geese import Action, row_col, greedy_agent
from kaggle_environments import make
import numpy as np
import torch.nn.functional as F

from network import Net

N_AGENTS = 4

N_ACTIONS = len(Action)
ACT2STR = {
    0: 'EAST',
    1: 'WEST',
    2: 'NORTH',
    3: 'SOUTH',
}
ACT2INT = {v: k for k, v in ACT2STR.items()}


class Agent:
    def __init__(self, conf):
        self.config = conf
        self.target_net = Net(N_ACTIONS).float().cuda()
        self.target_net.load_state_dict(torch.load('weights/best.pth'))
        self.last_obs = None

    def __call__(self, obs, conf):
        state = (self.process_map_state(obs), self.check_last_act(obs, self.last_obs))
        self.last_obs = obs
        action = int(self.select_action(state))
        return ACT2STR[action]

    def select_action(self, state):
        with torch.no_grad():
            return self.target_net(
                torch.Tensor(state[0]).permute(2, 0, 1).unsqueeze(0).cuda(),
                state[1].unsqueeze(0).cuda()
            ).max(1)[1].view(1, 1)

    def process_map_state(self, obs):
        geese = obs.geese
        opponents = [
            goose
            for index, goose in enumerate(geese)
            if index != obs.index and len(goose) > 0
        ]

        opponent_heads = [opponent[0] for opponent in opponents]
        bodies = {position for goose in geese for position in goose}
        position = geese[obs.index][0]

        depth = 3
        map = np.zeros((self.config.rows, self.config.columns, depth))

        for body in bodies:
            r, c = row_col(body, self.config.columns)
            map[r, c, 0] = 1
        for body in opponent_heads:
            r, c = row_col(body, self.config.columns)
            map[r, c, 1] = 1
        for body in obs.food:
            r, c = row_col(body, self.config.columns)
            map[r, c, 2] = 1
        r, c = row_col(position, self.config.columns)

        vertical_rolled = np.roll(map, int(self.config.rows / 2) - r, axis=0)
        centered_map = np.roll(vertical_rolled, int(self.config.columns / 2) - c, axis=1)
        return centered_map

    def check_last_act(self, obs, last_obs):
        if last_obs is None:
            return torch.zeros((N_AGENTS, 4))
        else:
            last_heads = [g[0] if len(g) else None for g in last_obs['geese']]
            heads = [g[0] if len(g) else None for g in obs['geese']]
            last_heads_coors = [None if h is None else row_col(h, self.config.columns) for h in last_heads]
            heads_coors = [None if h is None else row_col(h, self.config.columns) for h in heads]
            last_acts = []
            for i in range(N_AGENTS):
                head = heads_coors[i]
                last_head = last_heads_coors[i]

                try:
                    row_change = head[0] - last_head[0]
                except TypeError:
                    last_acts.append(-1)
                else:
                    try:
                        act = {
                            1: 'SOUTH',
                            -1: 'NORTH',
                            self.config.rows - 1: 'NORTH',
                            1 - self.config.rows: 'SOUTH',
                        }[row_change]
                    except KeyError:
                        col_change = head[1] - last_head[1]
                        act = {
                            1: 'EAST',
                            -1: 'WEST',
                            self.config.columns - 1: 'WEST',
                            1 - self.config.columns: 'EAST',
                        }[col_change]

                    last_acts.append(ACT2INT[act])
            last_acts = F.one_hot(
                torch.Tensor(np.array(last_acts) + 1).to(torch.int64),
                num_classes=N_AGENTS + 1)[:, 1:]
            return last_acts


def main():
    env = make("hungry_geese")
    agent = Agent(env.configuration)
    env.run([agent, greedy_agent, greedy_agent, greedy_agent])
    env.render(mode='ipython', width=800, height=600)


if __name__ == '__main__':
    main()
