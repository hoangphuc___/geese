import torch
from torch.nn import Module, Conv2d, Linear
import torch.nn.functional as F


class Net(Module):
    def __init__(self, n_actions):
        super().__init__()
        self.conv0 = Conv2d(3, 8, 3)
        self.conv1 = Conv2d(8, 16, 3)
        self.conv2 = Conv2d(16, 32, 3)
        self.fc0 = Linear(176, 64)
        self.fc0_drop = torch.nn.Dropout(0.25)
        self.fc1 = Linear(64, n_actions)

    def forward(self, x_map, x_act):
        x0 = F.relu(self.conv0(x_map))
        x1 = F.relu(self.conv1(x0))
        x2 = F.relu(self.conv2(x1))
        flat_x2 = x2.flatten(1)
        act_cat_x = torch.cat((flat_x2, x_act.flatten(1)), dim=1)
        fc0_out = F.relu(self.fc0_drop(self.fc0(act_cat_x)))
        out = self.fc1(fc0_out)

        return out
