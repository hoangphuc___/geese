from random import random, randrange

import mlflow as mlflow
import torch
from kaggle_environments.envs.hungry_geese.hungry_geese import Action, row_col, greedy_agent
from kaggle_environments import make
import numpy as np
from loguru import logger
from torch import optim
import torch.nn.functional as F

from network import Net
from replay_memory import ReplayMemory, Transition

N_AGENTS = 4

EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 200
N_ACTIONS = len(Action)
BATCH_SIZE = 512
GAMMA = 0.999
N_EPISODES = 50000
TARGET_UPDATE = 500
METRIC_LOG_INTERVAL = 1000
LR = 5e-2
ACT2STR = {
    0: 'EAST',
    1: 'WEST',
    2: 'NORTH',
    3: 'SOUTH',
}
ACT2INT = {v: k for k, v in ACT2STR.items()}


class Agent:
    def __init__(self, conf):
        self.config = conf
        self.memory = ReplayMemory(10000)
        self.policy_net = Net(N_ACTIONS).float().cuda()
        self.target_net = Net(N_ACTIONS).float().cuda()
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()
        self.optim = optim.RMSprop(self.policy_net.parameters(), lr=LR)
        self.global_steps = 0
        self.best_loss = None

    def select_action(self, state, eps_thresh):
        sample = random()
        if sample > eps_thresh:
            with torch.no_grad():
                return self.policy_net(
                    torch.Tensor(state[0]).permute(2, 0, 1).unsqueeze(0).cuda(),
                    state[1].unsqueeze(0).cuda()
                ).max(1)[1].view(1, 1)
        else:
            return torch.tensor([[randrange(N_ACTIONS)]])

    def process_map_state(self, obs):
        geese = obs.geese
        opponents = [
            goose
            for index, goose in enumerate(geese)
            if index != obs.index and len(goose) > 0
        ]

        opponent_heads = [opponent[0] for opponent in opponents]
        bodies = {position for goose in geese for position in goose}
        position = geese[obs.index][0]

        depth = 3
        map = np.zeros((self.config.rows, self.config.columns, depth))

        for body in bodies:
            r, c = row_col(body, self.config.columns)
            map[r, c, 0] = 1
        for body in opponent_heads:
            r, c = row_col(body, self.config.columns)
            map[r, c, 1] = 1
        for body in obs.food:
            r, c = row_col(body, self.config.columns)
            map[r, c, 2] = 1
        r, c = row_col(position, self.config.columns)

        vertical_rolled = np.roll(map, int(self.config.rows / 2) - r, axis=0)
        centered_map = np.roll(vertical_rolled, int(self.config.columns / 2) - c, axis=1)
        return centered_map

    def optimize_model(self):
        if len(self.memory) < BATCH_SIZE * 10:
            return
        transitions = self.memory.sample(BATCH_SIZE)
        batch = Transition(*zip(*transitions))

        non_final_mask = torch.tensor(tuple(map(lambda s: s is not None, batch.next_state)), dtype=torch.bool)
        non_final_next_map_states = torch.cat(
            [torch.from_numpy(s[0]).unsqueeze(0) for s in batch.next_state if s is not None]).permute(0, 3, 1, 2).to(
            torch.float32)
        non_final_next_act_states = torch.cat([s[1].unsqueeze(0) for s in batch.next_state if s is not None])
        map_state_batch = torch.cat([torch.from_numpy(s[0]).unsqueeze(0) for s in batch.state]).permute(0, 3, 1, 2).to(
            torch.float32)
        act_state_batch = torch.cat([s[1].unsqueeze(0) for s in batch.state if s is not None])
        action_batch = torch.Tensor(batch.action).to(torch.int64).unsqueeze(-1)
        reward_batch = torch.Tensor(batch.reward)

        state_action_values = self.policy_net(map_state_batch.cuda(), act_state_batch.cuda()).cpu().gather(1,
                                                                                                           action_batch)
        next_state_values = torch.zeros(BATCH_SIZE)
        next_state_values[non_final_mask] = self.target_net(
            non_final_next_map_states.cuda(), non_final_next_act_states.cuda()).cpu().max(1)[0].detach()
        expected_state_action_values = (next_state_values * GAMMA) + reward_batch

        loss = F.smooth_l1_loss(state_action_values, expected_state_action_values.unsqueeze(1))
        if self.global_steps % METRIC_LOG_INTERVAL == 0:
            try:
                if loss < self.best_loss:
                    self.best_loss = loss.data
                    mlflow.log_metric('best_loss', float(self.best_loss), self.global_steps)
            except TypeError:
                self.best_loss = loss.data
                mlflow.log_metric('best_loss', float(self.best_loss), self.global_steps)
            print(f' Best loss: {self.best_loss} - Loss: {loss}')
            mlflow.log_metric('loss', float(loss.data), self.global_steps)
        self.optim.zero_grad()
        loss.backward()
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-1, 1)

        self.optim.step()

    def evaluate_state(self, obs):
        opponent_max_length = max([len(g) for g in obs['geese'][1:]])
        geese_length = len(obs['geese'][0])
        if geese_length == 0:
            reward = -100
        else:
            reward = (geese_length - opponent_max_length) * 10

        return reward / 200

    def check_last_act(self, obs, last_obs):
        if last_obs is None:
            return torch.zeros((N_AGENTS, 4))
        else:
            last_heads = [g[0] if len(g) else None for g in last_obs['geese']]
            heads = [g[0] if len(g) else None for g in obs['geese']]
            last_heads_coors = [None if h is None else row_col(h, self.config.columns) for h in last_heads]
            heads_coors = [None if h is None else row_col(h, self.config.columns) for h in heads]
            last_acts = []
            for i in range(N_AGENTS):
                head = heads_coors[i]
                last_head = last_heads_coors[i]

                try:
                    row_change = head[0] - last_head[0]
                except TypeError:
                    last_acts.append(-1)
                else:
                    try:
                        act = {
                            1: 'SOUTH',
                            -1: 'NORTH',
                            self.config.rows - 1: 'NORTH',
                            1 - self.config.rows: 'SOUTH',
                        }[row_change]
                    except KeyError:
                        col_change = head[1] - last_head[1]
                        act = {
                            1: 'EAST',
                            -1: 'WEST',
                            self.config.columns - 1: 'WEST',
                            1 - self.config.columns: 'EAST',
                        }[col_change]

                    last_acts.append(ACT2INT[act])
            last_acts = F.one_hot(
                torch.Tensor(np.array(last_acts) + 1).to(torch.int64),
                num_classes=N_AGENTS + 1)[:, 1:]
            return last_acts


def main():
    env = make("hungry_geese")
    agent = Agent(env.configuration)
    trainer = env.train([None, greedy_agent, greedy_agent, greedy_agent])
    mlflow.set_experiment('geese')
    mlflow.start_run()
    mlflow.log_params({
        'target_net_update_interval': TARGET_UPDATE,
        'lr': LR
    })
    best_reward = None
    for i_episode in range(N_EPISODES):
        trainer.reset()

        obs = env.state[0].observation
        last_obs = None
        while True:
            try:
                state = next_state
            except NameError:
                state = (agent.process_map_state(obs), agent.check_last_act(obs, last_obs))

            last_obs = obs
            eps_thresh = EPS_START - i_episode / N_EPISODES * (EPS_START - EPS_END)
            if agent.global_steps % METRIC_LOG_INTERVAL == 0:
                mlflow.log_metric('eps', eps_thresh, agent.global_steps)

            action = int(agent.select_action(state, eps_thresh))
            obs, _, done, info = trainer.step(ACT2STR[action])
            reward = agent.evaluate_state(obs)
            try:
                if reward > best_reward:
                    best_reward = reward
            except TypeError:
                best_reward = reward

            mlflow.log_metric('best_metric', max(0, best_reward), agent.global_steps)
            mlflow.log_metric('reward', max(0, reward), agent.global_steps)
            if done:
                agent.memory.push(state, action, None, reward)
                break
            else:
                next_state = (agent.process_map_state(obs), agent.check_last_act(obs, last_obs))
                agent.memory.push(state, action, next_state, reward)

            agent.optimize_model()
            agent.global_steps += 1

        if (i_episode + 1) % TARGET_UPDATE == 0:
            agent.target_net.load_state_dict(agent.policy_net.state_dict())
            logger.info('Target network updated')
            torch.save(agent.target_net.state_dict(), 'weights/best.pth')


if __name__ == '__main__':
    main()
